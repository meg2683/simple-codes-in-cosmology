# Simple codes in cosmology

Simple codes will be uploaded here that are useful to study cosmology. For example the codes will calculate the age of the Universe, compute different distance measures based on the standard model of cosmology. I plan to upload codes at research level too at some point of time. 

The first code will calculate the age of the Universe based on the best fit parameters obtained using the data from the Planck mission (https://www.cosmos.esa.int/web/planck).  

---
* Developed by Dhiraj Kumar Hazra, IMSc, Chennai, India
* for questions, please write to dhirajhazra@gmail.com
